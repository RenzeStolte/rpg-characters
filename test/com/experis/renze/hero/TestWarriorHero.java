package com.experis.renze.hero;

import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.hero.specializations.WarriorSpecialization;
import com.experis.renze.enums.StatType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestWarriorHero {

    ISpecialization warriorClass = new WarriorSpecialization();
    IHero warriorHero = new Hero(warriorClass, 1, "warrior", new EquipmentHandler());

    /**
     * Test that you gain HP when you level up, and that current HP also gets updated.
     */
    @Test
    public void testHealthGain() {
        assertEquals(150, warriorHero.getTotalStat(StatType.HLT));
        assertEquals(150, warriorHero.getCurrentHealth());
        warriorHero.gainXP(100);
        assertEquals(180, warriorHero.getTotalStat(StatType.HLT));
        assertEquals(180, warriorHero.getCurrentHealth());
        warriorHero.gainXP(110);
        assertEquals(210, warriorHero.getTotalStat(StatType.HLT));
        assertEquals(210, warriorHero.getCurrentHealth());
    }

    /**
     * Test that you gain strength when you level up.
     */
    @Test
    public void testStrengthGain() {
        assertEquals(10, warriorHero.getTotalStat(StatType.STR));
        warriorHero.gainXP(100);
        assertEquals(15, warriorHero.getTotalStat(StatType.STR));
        warriorHero.gainXP(110);
        assertEquals(20, warriorHero.getTotalStat(StatType.STR));
    }

    /**
     * Test that you gain dexterity when you level up.
     */
    @Test
    public void testDexGain() {
        assertEquals(3, warriorHero.getTotalStat(StatType.DEX));
        warriorHero.gainXP(100);
        assertEquals(5, warriorHero.getTotalStat(StatType.DEX));
        warriorHero.gainXP(110);
        assertEquals(7, warriorHero.getTotalStat(StatType.DEX));
    }

    /**
     * Test that you gain intelligence when you level up.
     */
    @Test
    public void testIntelligenceGain() {
        assertEquals(1, warriorHero.getTotalStat(StatType.INT));
        warriorHero.gainXP(100);
        assertEquals(2, warriorHero.getTotalStat(StatType.INT));
        warriorHero.gainXP(110);
        assertEquals(3, warriorHero.getTotalStat(StatType.INT));
    }

    @Test
    public void testGetName() {
        assertEquals("warrior", warriorHero.getName());
    }
}
