package com.experis.renze.hero;

import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.hero.specializations.MageSpecialization;
import com.experis.renze.enums.StatType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMageHero {

    ISpecialization mageClass = new MageSpecialization();
    IHero mageHero = new Hero(mageClass, 1, "mage", new EquipmentHandler());

    /**
     * Test that you gain HP when you level up, and that current HP also gets updated.
     */
    @Test
    public void testHealthGain() {
        assertEquals(100, mageHero.getTotalStat(StatType.HLT));
        assertEquals(100, mageHero.getCurrentHealth());
        mageHero.gainXP(100);
        assertEquals(115, mageHero.getTotalStat(StatType.HLT));
        assertEquals(115, mageHero.getCurrentHealth());
        mageHero.gainXP(110);
        assertEquals(130, mageHero.getTotalStat(StatType.HLT));
        assertEquals(130, mageHero.getCurrentHealth());
    }

    /**
     * Test that you gain strength when you level up.
     */
    @Test
    public void testStrengthGain() {
        assertEquals(2, mageHero.getTotalStat(StatType.STR));
        mageHero.gainXP(100);
        assertEquals(3, mageHero.getTotalStat(StatType.STR));
        mageHero.gainXP(110);
        assertEquals(4, mageHero.getTotalStat(StatType.STR));
    }

    /**
     * Test that you gain dexterity when you level up.
     */
    @Test
    public void testDexGain() {
        assertEquals(3, mageHero.getTotalStat(StatType.DEX));
        mageHero.gainXP(100);
        assertEquals(5, mageHero.getTotalStat(StatType.DEX));
        mageHero.gainXP(110);
        assertEquals(7, mageHero.getTotalStat(StatType.DEX));
    }

    /**
     * Test that you gain intelligence when you level up.
     */
    @Test
    public void testIntelligenceGain() {
        assertEquals(10, mageHero.getTotalStat(StatType.INT));
        mageHero.gainXP(100);
        assertEquals(15, mageHero.getTotalStat(StatType.INT));
        mageHero.gainXP(110);
        assertEquals(20, mageHero.getTotalStat(StatType.INT));
    }

    @Test
    public void testGetName() {
        assertEquals("mage", mageHero.getName());
    }
}
