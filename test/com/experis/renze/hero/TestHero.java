package com.experis.renze.hero;

import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.hero.specializations.MageSpecialization;
import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.*;
import com.experis.renze.weapon.types.MagicWeapon;
import com.experis.renze.weapon.types.MeleeWeapon;
import com.experis.renze.weapon.types.RangedWeapon;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class TestHero {

    IHero hero = new Hero(new MageSpecialization(), 1, "mage", new EquipmentHandler());

    @Test
    public void testLevelUp(){
        hero.gainXP(100);
        assertEquals(2,hero.getLevel());
        hero.gainXP(100);
        assertEquals(2, hero.getLevel());
        hero.gainXP(10);
        assertEquals(3, hero.getLevel());
        hero.gainXP(110);
        assertEquals(3, hero.getLevel());
        hero.gainXP(11);
        assertEquals(4, hero.getLevel());
    }

    @Test
    public void testHealth() {
        assertTrue(hero.isAlive());
        assertEquals(hero.getTotalStat(StatType.HLT), hero.getCurrentHealth());
        hero.takeDamage(1);
        assertEquals(hero.getTotalStat(StatType.HLT) - 1, hero.getCurrentHealth());
        assertTrue(hero.isAlive());
        hero.takeDamage(2);
        assertEquals(hero.getTotalStat(StatType.HLT) - 3, hero.getCurrentHealth());
        assertTrue(hero.isAlive());
        hero.takeDamage(hero.getCurrentHealth());
        assertFalse(hero.isAlive());
    }

    @Test
    public void testAttackPower() {
        assertEquals(0, hero.getAttackPower());

        IWeapon magicWeapon = new Weapon(1, "magic weapon", new MagicWeapon());
        hero.equipWeapon(magicWeapon);
        assertEquals(hero.getTotalStat(StatType.INT)*3 + magicWeapon.getAttackPower(), hero.getAttackPower());

        IWeapon meleeWeapon = new Weapon(1, "melee weapon", new MeleeWeapon());
        hero.equipWeapon(meleeWeapon);
        assertEquals((int) (hero.getTotalStat(StatType.STR) * 1.5) + meleeWeapon.getAttackPower(), hero.getAttackPower());

        IWeapon rangedWeapon = new Weapon(1, "ranged weapon", new RangedWeapon());
        hero.equipWeapon(rangedWeapon);
        assertEquals(hero.getTotalStat(StatType.DEX) * 2 + rangedWeapon.getAttackPower(), hero.getAttackPower());
    }
}
