package com.experis.renze.hero;

import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.hero.specializations.RangerSpecialization;
import com.experis.renze.enums.StatType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestRangerHero {

    ISpecialization rangerClass = new RangerSpecialization();
    IHero rangerHero = new Hero(rangerClass, 1, "ranger", new EquipmentHandler());

    /**
     * Test that you gain HP when you level up, and that current HP also gets updated.
     */
    @Test
    public void testHealthGain() {
        assertEquals(120, rangerHero.getTotalStat(StatType.HLT));
        assertEquals(120, rangerHero.getCurrentHealth());
        rangerHero.gainXP(100);
        assertEquals(140, rangerHero.getTotalStat(StatType.HLT));
        assertEquals(140, rangerHero.getCurrentHealth());
        rangerHero.gainXP(110);
        assertEquals(160, rangerHero.getTotalStat(StatType.HLT));
        assertEquals(160, rangerHero.getCurrentHealth());

    }

    /**
     * Test that you gain strength when you level up.
     */
    @Test
    public void testStrengthGain() {
        assertEquals(5, rangerHero.getTotalStat(StatType.STR));
        rangerHero.gainXP(100);
        assertEquals(7, rangerHero.getTotalStat(StatType.STR));
        rangerHero.gainXP(110);
        assertEquals(9, rangerHero.getTotalStat(StatType.STR));
    }

    /**
     * Test that you gain dexterity when you level up.
     */
    @Test
    public void testDexGain() {
        assertEquals(10, rangerHero.getTotalStat(StatType.DEX));
        rangerHero.gainXP(100);
        assertEquals(15, rangerHero.getTotalStat(StatType.DEX));
        rangerHero.gainXP(110);
        assertEquals(20, rangerHero.getTotalStat(StatType.DEX));
    }

    /**
     * Test that you gain intelligence when you level up.
     */
    @Test
    public void testIntelligenceGain() {
        assertEquals(2, rangerHero.getTotalStat(StatType.INT));
        rangerHero.gainXP(100);
        assertEquals(3, rangerHero.getTotalStat(StatType.INT));
        rangerHero.gainXP(110);
        assertEquals(4, rangerHero.getTotalStat(StatType.INT));
    }

    @Test
    public void testGetName() {
        assertEquals("ranger", rangerHero.getName());
    }

}
