package com.experis.renze.hero;

import com.experis.renze.armor.*;
import com.experis.renze.armor.specializations.ClothArmor;
import com.experis.renze.armor.specializations.LeatherArmor;
import com.experis.renze.armor.specializations.PlateArmor;
import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.hero.inventory.IEquipmentHandler;
import com.experis.renze.weapon.*;
import com.experis.renze.enums.ItemSlot;
import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.types.MagicWeapon;
import com.experis.renze.weapon.types.MeleeWeapon;
import com.experis.renze.weapon.types.RangedWeapon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestEquipmentHandler {

    IEquipmentHandler equipmentHandler = new EquipmentHandler();


    IArmor leatherHelm = new Armor(new LeatherArmor(), 1, ItemSlot.HEAD, "desolate leather helm");
    IArmor leatherLegs = new Armor(new LeatherArmor(), 1, ItemSlot.LEGS, "desolate leather leggings");
    IArmor leatherBody = new Armor(new LeatherArmor(), 1, ItemSlot.BODY, "desolate leather vest");
    IArmor plateHead = new Armor(new PlateArmor(), 1, ItemSlot.HEAD, "chest plate");
    IArmor clothBody = new Armor(new ClothArmor(), 1, ItemSlot.BODY, "shirt");

    IWeapon wand = new Weapon(1, "wand", new MagicWeapon());
    IWeapon axe = new Weapon(1, "axe", new MeleeWeapon());
    IWeapon bow = new Weapon(1, "bow", new RangedWeapon());

    /**
     * Tests that you can equip armor, and that armor gets replaced when a piece in the same slot gets equipped.
     */
    @Test
    public void testEquippingItems() {
        // Test that all slots return null when no equipment is equipped
        assertEquals(null, equipmentHandler.getArmorInSlot(ItemSlot.BODY));
        assertEquals(null, equipmentHandler.getArmorInSlot(ItemSlot.LEGS));
        assertEquals(null, equipmentHandler.getArmorInSlot(ItemSlot.HEAD));

        equipmentHandler.equipArmor(leatherHelm);
        assertEquals(leatherHelm, equipmentHandler.getArmorInSlot(ItemSlot.HEAD));

        equipmentHandler.equipArmor(leatherLegs);
        assertEquals(leatherLegs, equipmentHandler.getArmorInSlot(ItemSlot.LEGS));

        equipmentHandler.equipArmor(leatherBody);
        assertEquals(leatherBody, equipmentHandler.getArmorInSlot(ItemSlot.BODY));

        equipmentHandler.equipArmor(clothBody);
        assertEquals(clothBody, equipmentHandler.getArmorInSlot(ItemSlot.BODY));

        equipmentHandler.equipArmor(plateHead);
        assertEquals(plateHead, equipmentHandler.getArmorInSlot(ItemSlot.HEAD));
    }

    /**
     * Tests that you can equip a weapon, and if you equip another weapon it gets replaced.
     */
    @Test
    public void equipWeapon() {
        equipmentHandler.equipWeapon(wand);
        assertEquals(wand, equipmentHandler.getWeapon());

        equipmentHandler.equipWeapon(bow);
        assertEquals(bow, equipmentHandler.getWeapon());

        equipmentHandler.equipWeapon(axe);
        assertEquals(axe, equipmentHandler.getWeapon());
    }

    /**
     * Tests that the equipmentHandler adds the sum of stats correctly.
     */
    @Test
    public void sumOfStats() {
        equipmentHandler.equipArmor(leatherBody);
        equipmentHandler.equipArmor(leatherLegs);
        equipmentHandler.equipArmor(leatherHelm);
        assertEquals(20 + (int) (20 * 0.8) + (int) (20 * 0.6), equipmentHandler.getStatPower(StatType.HLT));
        assertEquals(0, equipmentHandler.getStatPower(StatType.INT));
        assertEquals(3 + (int) (3 * 0.8) + (int) (3 * 0.6), equipmentHandler.getStatPower(StatType.DEX));
        assertEquals(1, equipmentHandler.getStatPower(StatType.STR));

        equipmentHandler.equipArmor(clothBody);
        assertEquals(10 + (int) (20 * 0.8) + (int) (20 * 0.6), equipmentHandler.getStatPower(StatType.HLT));
        assertEquals(3, equipmentHandler.getStatPower(StatType.INT));
        assertEquals(1 + (int) (3 * 0.8) + (int) (3 * 0.6), equipmentHandler.getStatPower(StatType.DEX));
        assertEquals(0, equipmentHandler.getStatPower(StatType.STR));

        equipmentHandler.equipArmor(plateHead);
        assertEquals(10 + (int) (30 * 0.8) + (int) (20 * 0.6), equipmentHandler.getStatPower(StatType.HLT));
        assertEquals(3, equipmentHandler.getStatPower(StatType.INT));
        assertEquals(1 + (int) (1 * 0.8) + (int) (3 * 0.6), equipmentHandler.getStatPower(StatType.DEX));
        assertEquals((int) (3 * 0.8), equipmentHandler.getStatPower(StatType.STR));
    }

}
