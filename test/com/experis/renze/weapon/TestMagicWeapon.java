package com.experis.renze.weapon;

import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.types.MagicWeapon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMagicWeapon {

    int baseStat = 25;
    int scaleStat = 2;

    IWeapon magicWeaponOne = new Weapon(1, "magic weapon", new MagicWeapon());
    IWeapon magicWeaponTwo = new Weapon(2, "magic weapon", new MagicWeapon());
    IWeapon magicWeaponThree = new Weapon(3, "magic weapon", new MagicWeapon());

    @Test
    public void testAttackPower() {
        assertEquals(baseStat, magicWeaponOne.getAttackPower());
        assertEquals(baseStat + scaleStat, magicWeaponTwo.getAttackPower());
        assertEquals(baseStat + scaleStat * 2, magicWeaponThree.getAttackPower());
    }

    @Test
    public void testGetAttackStat(){
        assertEquals(StatType.INT, magicWeaponOne.getAttackStat());
    }

    @Test
    public void testGetStatMultiplier() {
        assertEquals(3, magicWeaponOne.getStatMultiplier(), 0.001);
        assertEquals(3, magicWeaponTwo.getStatMultiplier(), 0.001);
        assertEquals(3, magicWeaponThree.getStatMultiplier(), 0.001);
    }
}

