package com.experis.renze.weapon;

import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.types.MeleeWeapon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMeleeWeapon {
    int baseStat = 15;
    int scaleStat = 2;

    IWeapon meleeWeaponOne = new Weapon(1, "melee weapon", new MeleeWeapon());
    IWeapon meleeWeaponTwo = new Weapon(2, "melee weapon", new MeleeWeapon());
    IWeapon meleeWeaponThree = new Weapon(3, "melee weapon", new MeleeWeapon());

    @Test
    public void testAttackPower() {
        assertEquals(baseStat, meleeWeaponOne.getAttackPower());
        assertEquals(baseStat + scaleStat, meleeWeaponTwo.getAttackPower());
        assertEquals(baseStat + scaleStat * 2, meleeWeaponThree.getAttackPower());
    }

    @Test
    public void testGetAttackStat(){
        assertEquals(StatType.STR, meleeWeaponOne.getAttackStat());
    }

    @Test
    public void testGetStatMultiplier() {
        assertEquals(1.5, meleeWeaponOne.getStatMultiplier(), 0.001);
        assertEquals(1.5, meleeWeaponTwo.getStatMultiplier(), 0.001);
        assertEquals(1.5, meleeWeaponThree.getStatMultiplier(), 0.001);
    }
}
