package com.experis.renze.weapon;

import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.types.RangedWeapon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestRangedWeapon {
    int baseStat = 5;
    int scaleStat = 3;

    IWeapon rangedWeaponOne = new Weapon(1, "ranged weapon", new RangedWeapon());
    IWeapon rangedWeaponTwo = new Weapon(2, "ranged weapon", new RangedWeapon());
    IWeapon rangedWeaponThree = new Weapon(3, "ranged weapon", new RangedWeapon());

    @Test
    public void testAttackPower() {
        assertEquals(baseStat, rangedWeaponOne.getAttackPower());
        assertEquals(baseStat + scaleStat, rangedWeaponTwo.getAttackPower());
        assertEquals(baseStat + scaleStat * 2, rangedWeaponThree.getAttackPower());
    }

    @Test
    public void testGetAttackStat(){
        assertEquals(StatType.DEX, rangedWeaponOne.getAttackStat());
    }

    @Test
    public void testGetStatMultiplier() {
        assertEquals(2, rangedWeaponOne.getStatMultiplier(), 0.001);
        assertEquals(2, rangedWeaponTwo.getStatMultiplier(), 0.001);
        assertEquals(2, rangedWeaponThree.getStatMultiplier(), 0.001);
    }
}
