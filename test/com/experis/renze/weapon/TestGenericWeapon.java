package com.experis.renze.weapon;

import com.experis.renze.weapon.types.MagicWeapon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGenericWeapon {

    IWeapon weapon = new Weapon(1, "weapon", new MagicWeapon());
    IWeapon betterWeapon = new Weapon(4, "stronk weapon", new MagicWeapon());

    @Test
    public void testGetName() {
        assertEquals("weapon", weapon.getItemName());
        assertEquals("stronk weapon", betterWeapon.getItemName());
    }

    @Test
    public void testGetLevel() {
        assertEquals(1, weapon.getLevel());
        assertEquals(4, betterWeapon.getLevel());
    }
}

