package com.experis.renze.armor;

import com.experis.renze.armor.specializations.PlateArmor;
import com.experis.renze.enums.ItemSlot;
import com.experis.renze.enums.StatType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestPlateArmor {

    IArmor levelOneBodyArmor = new Armor(new PlateArmor(), 1, ItemSlot.BODY, "plate armor");
    IArmor levelTwoBodyArmor = new Armor(new PlateArmor(), 2, ItemSlot.BODY, "plate armor");
    IArmor levelThreeBodyArmor = new Armor(new PlateArmor(), 3, ItemSlot.BODY, "plate armor");
    IArmor getLevelOneLegArmor = new Armor(new PlateArmor(), 1, ItemSlot.LEGS, "plate armor");
    IArmor getLevelTwoLegArmor = new Armor(new PlateArmor(), 2, ItemSlot.LEGS, "plate armor");
    IArmor getLevelThreeLegArmor = new Armor(new PlateArmor(), 3, ItemSlot.LEGS, "plate armor");
    IArmor getLevelOneHeadArmor = new Armor(new PlateArmor(), 1, ItemSlot.HEAD, "plate armor");
    IArmor getLevelTwoHeadArmor = new Armor(new PlateArmor(), 2, ItemSlot.HEAD, "plate armor");
    IArmor getLevelThreeHeadArmor = new Armor(new PlateArmor(), 3, ItemSlot.HEAD, "plate armor");

    int startHP = 30;
    int startInt = 0;
    int startDex = 1;
    int startStrength = 3;

    int scaleHP = 12;
    int scaleInt = 0;
    int scaleDex = 1;
    int scaleStrength = 2;

    double headScale = 0.8;
    double legScale = 0.6;


    /**
     * Tests correct stat on plate items for level 1-3
     */
    @Test
    public void testCorrectLevelStats() {
        assertEquals(startStrength, levelOneBodyArmor.getStat(StatType.STR));
        assertEquals(startStrength + scaleStrength, levelTwoBodyArmor.getStat(StatType.STR));
        assertEquals(startStrength + scaleStrength * 2, levelThreeBodyArmor.getStat(StatType.STR));
        assertEquals(startDex, levelOneBodyArmor.getStat(StatType.DEX));
        assertEquals(startDex + scaleDex, levelTwoBodyArmor.getStat(StatType.DEX));
        assertEquals(startDex + scaleDex * 2, levelThreeBodyArmor.getStat(StatType.DEX));
        assertEquals(startInt, levelOneBodyArmor.getStat(StatType.INT));
        assertEquals(startInt + scaleInt, levelTwoBodyArmor.getStat(StatType.INT));
        assertEquals(startInt + scaleInt * 2, levelThreeBodyArmor.getStat(StatType.INT));
        assertEquals(startHP, levelOneBodyArmor.getStat(StatType.HLT));
        assertEquals(startHP + scaleHP, levelTwoBodyArmor.getStat(StatType.HLT));
        assertEquals(startHP + scaleHP * 2, levelThreeBodyArmor.getStat(StatType.HLT));
    }

    /**
     * Tests correct stat for items in slot head and leg slots.
     * Only checks the primary stat.
     */
    @Test
    public void testScalingOfSlots() {
        // All the scaling for int stats
        assertEquals((int) (headScale * startInt), getLevelOneHeadArmor.getStat(StatType.INT));
        assertEquals((int) (headScale * (startInt + scaleInt)), getLevelTwoHeadArmor.getStat(StatType.INT));
        assertEquals((int) (headScale * (startInt + scaleInt * 2)), getLevelThreeHeadArmor.getStat(StatType.INT));
        assertEquals((int) (legScale * startInt), getLevelOneLegArmor.getStat(StatType.INT));
        assertEquals((int) (legScale * (startInt + scaleInt)), getLevelTwoLegArmor.getStat(StatType.INT));
        assertEquals((int) (legScale * (startInt + scaleInt * 2)), getLevelThreeLegArmor.getStat(StatType.INT));
    }
}
