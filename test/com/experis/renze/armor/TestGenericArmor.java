package com.experis.renze.armor;

import com.experis.renze.armor.specializations.ClothArmor;
import com.experis.renze.armor.specializations.LeatherArmor;
import com.experis.renze.armor.specializations.PlateArmor;
import com.experis.renze.enums.ItemSlot;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGenericArmor {
    IArmor armor;

    @Test
    public void testGetName() {
        armor = new Armor(new ClothArmor(), 1, ItemSlot.BODY, "cloth armor");
        assertEquals("cloth armor", armor.getArmorName());

        armor = new Armor(new LeatherArmor(), 1, ItemSlot.BODY, "leather armor");
        assertEquals("leather armor", armor.getArmorName());

        armor = new Armor(new PlateArmor(), 1, ItemSlot.BODY, "plate armor");
        assertEquals("plate armor", armor.getArmorName());
    }

    @Test
    public void testGetSlotType() {
        armor = new Armor(new ClothArmor(), 1, ItemSlot.BODY ,"cloth armor");
        assertEquals(ItemSlot.BODY, armor.getSlotType());
        armor = new Armor(new ClothArmor(), 1, ItemSlot.HEAD, "cloth armor");
        assertEquals(ItemSlot.HEAD, armor.getSlotType());
        armor = new Armor(new ClothArmor(), 1, ItemSlot.LEGS, "cloth armor");
        assertEquals(ItemSlot.LEGS, armor.getSlotType());
    }

    @Test
    public void testGetLevel() {
        armor = new Armor(new ClothArmor(), 1, ItemSlot.BODY, "cloth armor");
        assertEquals(1, armor.getLevel());
        armor = new Armor(new ClothArmor(), 2, ItemSlot.BODY, "cloth armor");
        assertEquals(2, armor.getLevel());
        armor = new Armor(new ClothArmor(), 3, ItemSlot.BODY, "cloth armor");
        assertEquals(3, armor.getLevel());
        armor = new Armor(new ClothArmor(), 4, ItemSlot.BODY, "cloth armor");
        assertEquals(4, armor.getLevel());
    }
}
