# RPG Characters

# Overview
A simple modelling of an RPG character system with a demonstration of the features working. It includes different types of Hero's, weapons and equipment. Everything has been made with the SOLID principles in mind, and the code should be easily extendable.
Includes two options for the user to show functionalities: A demo and a duel mode.

## Hero
A Hero consists of two parts; An equipment handler and a class specialization. 
- The class specialization control how many stats the Hero gets at the start and whenever they level up. The three implemented class specialization are Warrior, Mage and Ranger. 
- The equipment handler is responsible for keeping track of the equipment the hero has, which includes both weapons and armor.
- Hero has other functionalities like max health, current health, isAlive, and attack power. This is calculated by polling equipped items through the equipment handler.

## Armor
- Armor, like Hero, also has different specializations. This determines how many stats they have at level one and when it's level increases. The three armor specializations implemented are cloth armor, leather armor and plate armor.  
- Armor can be equipped in three different slots; Head, legs and body. Armor equipped in the body slot give 100% effectiveness when calculating stats, while armor in the head and legs slot give 80% and 60%, respectively. 

## Weapons
Weapons have a base attack power stat, and determine which stat is used to calculate damage. There are three different weapon types implemented: Ranged, Melee and Magic weapon. 
<br />The formula for damage is as follows:
<br />Melee attacks: attack power + (strength * 1.5)
<br />Ranged attacks: attack power + (dexterity * 2)
<br />Melee attacks: attack power + (magic * 3)

### Demonstration
Generates a hero, weapons and armors and shows their stats. Levels a hero, and equips them with the items, and shows that stats change correctly. 

### Duel mode
Lets the user create a hero. Then lets the user choose between three options two times:
<br /> 1 - Heal to full
<br /> 2 - Get a new, randomly generated piece of armor which they can choose to equip. 
<br /> 3 - Get a new, randomly generated weapon which they can choose to equip. 
<br />
After this, the user battles against a randomly generated AI. The AI has a random class and items and scales with the level of the user's hero. If the user defeat the AI, they get 25 xp and they go back to being able to take two of the three options. If the user dies, the game ends.
