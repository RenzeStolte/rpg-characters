package com.experis.renze.hero;

import com.experis.renze.enums.StatType;

import java.util.HashMap;

public interface ISpecialization {
    /**
     * Sets the base stats for level one in the HashMap stats
     */
    public void setLevelOneStats(HashMap<StatType, Integer> stats);

    /**
     * Levels all stats up nOfLevels times by adding to the HashMap stats.
     */
    public void levelStats(HashMap<StatType, Integer> stats, int nOfLevels);

    /**
     * Returns the name of the specialization.
     */
    public String getName();
}
