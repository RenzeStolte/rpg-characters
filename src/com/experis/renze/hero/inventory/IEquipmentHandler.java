package com.experis.renze.hero.inventory;

import com.experis.renze.armor.IArmor;
import com.experis.renze.enums.ItemSlot;
import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.IWeapon;

public interface IEquipmentHandler {

    /**
     * Equip the specified armor.
     */
    public void equipArmor(IArmor armor);

    /**
     * Equip the specified weapon
     */
    public void equipWeapon(IWeapon weapon);

    /**
     * Returns the armor that is equipped in the specified slot.
     * Returns null if there is no armor equipped in that slot.
     */
    public IArmor getArmorInSlot(ItemSlot slot);

    /**
     * Returns the currently equipped weapon.
     * Returns null if no weapon has been equipped.
     */
    public IWeapon getWeapon();

    /**
     * Gets the sum of all armor's stats. Only returns the requested stat.
     */
    public int getStatPower(StatType statType);

}
