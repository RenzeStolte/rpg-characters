package com.experis.renze.hero.inventory;

import com.experis.renze.armor.IArmor;
import com.experis.renze.weapon.IWeapon;
import com.experis.renze.enums.ItemSlot;
import com.experis.renze.enums.StatType;
import jdk.jfr.Percentage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EquipmentHandler implements IEquipmentHandler{

    HashMap<ItemSlot, IArmor> list;
    IWeapon weapon;

    public EquipmentHandler(){
        list = new HashMap<>();
    }

    @Override
    public int getStatPower(StatType statType){
        int sum = 0;
        for(ItemSlot slot : ItemSlot.values()) {
            if(getArmorInSlot(slot) != null)
                sum += getArmorInSlot(slot).getStat(statType);
        }
        return sum;
    }

    @Override
    public void equipArmor(IArmor armor) {
        list.put(armor.getSlotType(), armor);
    }

    @Override
    public IArmor getArmorInSlot(ItemSlot slot) {
        return list.get(slot);
    }

    @Override
    public void equipWeapon(IWeapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public IWeapon getWeapon() {
        return weapon;
    }
}
