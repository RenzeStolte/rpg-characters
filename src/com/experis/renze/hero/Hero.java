package com.experis.renze.hero;

import com.experis.renze.armor.IArmor;
import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.hero.inventory.IEquipmentHandler;
import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.IWeapon;

import java.util.HashMap;

public class Hero implements IHero{

    HashMap<StatType, Integer> stats;
    ISpecialization heroClass;
    IEquipmentHandler equipmentHandler;
    String name;
    boolean isAlive;
    int xpToNextLevel;
    int xp;
    int level;
    int currentHealth;

    public Hero(ISpecialization heroClass, int level, String name, IEquipmentHandler equipmentHandler) {
        // Set local variables
        this.level = level;
        this.heroClass = heroClass;
        this.name = name;
        this.equipmentHandler = equipmentHandler;

        // Initiate local variables
        this.stats = new HashMap<StatType, Integer>();
        this.isAlive = true;
        this.xpToNextLevel = (int) (100 * Math.pow(1.1, level - 1));

        // Set stats and health
        heroClass.setLevelOneStats(stats);
        heroClass.levelStats(stats, level - 1);
        this.currentHealth = getTotalStat(StatType.HLT);
    }

    @Override
    public String className() {
        return heroClass.getName();
    }

    @Override
    public void gainXP(int xp) {
        this.xp += xp;
        checkToLevelUp();
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public void equipWeapon(IWeapon weapon) {
        equipmentHandler.equipWeapon(weapon);
    }

    @Override
    public void equipArmor(IArmor armor) {
        // Make sure that currentHealth gets adjusted based on how much HP stat was gained/lost
        int hpBefore = getTotalStat(StatType.HLT);
        equipmentHandler.equipArmor(armor);
        int hpAfter = getTotalStat(StatType.HLT);

        currentHealth += (hpAfter - hpBefore);
    }

    @Override
    public int getAttackPower() {
        IWeapon equippedWeapon = equipmentHandler.getWeapon();
        if(equippedWeapon == null) return 0;
        StatType activeStat = equippedWeapon.getAttackStat();
        return (int) (equippedWeapon.getAttackPower() + getTotalStat(activeStat) * equippedWeapon.getStatMultiplier());
    }

    @Override
    public int getCurrentHealth() {
        return currentHealth;
    }

    @Override
    public int getTotalStat(StatType statType) {
        return stats.get(statType) + equipmentHandler.getStatPower(statType);
    }

    /**
     * Loses hp equal to damage. If hit to zero or less, the character is no longer alive
     * and its status is updated accordingly.
     */
    @Override
    public void takeDamage(int damage) {
        currentHealth -= damage;
        if(currentHealth <= 0) {
            isAlive = false;
        }
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void healToFull() {
        currentHealth = getTotalStat(StatType.HLT);
    }

    /**
     * Checks if the hero has gained enough xp to level up.
     */
    private void checkToLevelUp() {
        if(xp >= xpToNextLevel) {
            levelUp();
        }
    }

    /**
     * Subtracts the xp required to level up, sets new xp requirement for next level up,
     * then runs another checkToLevelUp check.
     * Also heals the Hero to full.
     */
    private void levelUp() {
        level++;
        xp -= xpToNextLevel;
        xpToNextLevel = (int) (xpToNextLevel * 1.1);
        heroClass.levelStats(stats, 1);
        healToFull();
        checkToLevelUp();
    }
}
