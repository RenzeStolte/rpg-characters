package com.experis.renze.hero.specializations;

import com.experis.renze.hero.ISpecialization;
import com.experis.renze.enums.StatType;

import java.util.HashMap;

public class MageSpecialization implements ISpecialization {
    @Override
    public void setLevelOneStats(HashMap<StatType, Integer> stats) {
        stats.put(StatType.HLT, 100);
        stats.put(StatType.STR, 2);
        stats.put(StatType.DEX, 3);
        stats.put(StatType.INT, 10);
    }

    @Override
    public void levelStats(HashMap<StatType, Integer> stats, int nOfLevels) {
        stats.put(StatType.HLT, stats.get(StatType.HLT) + nOfLevels * 15);
        stats.put(StatType.STR, stats.get(StatType.STR) + nOfLevels * 1);
        stats.put(StatType.DEX, stats.get(StatType.DEX) + nOfLevels * 2);
        stats.put(StatType.INT, stats.get(StatType.INT) + nOfLevels * 5);
    }

    @Override
    public String getName() {
        return "Mage";
    }
}
