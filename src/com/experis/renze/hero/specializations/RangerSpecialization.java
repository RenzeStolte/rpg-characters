package com.experis.renze.hero.specializations;

import com.experis.renze.hero.ISpecialization;
import com.experis.renze.enums.StatType;

import java.util.HashMap;

public class RangerSpecialization implements ISpecialization {
    @Override
    public void setLevelOneStats(HashMap<StatType, Integer> stats) {
        stats.put(StatType.HLT, 120);
        stats.put(StatType.STR, 5);
        stats.put(StatType.DEX, 10);
        stats.put(StatType.INT, 2);
    }

    @Override
    public void levelStats(HashMap<StatType, Integer> stats, int nOfLevels) {
        stats.put(StatType.HLT, stats.get(StatType.HLT) + nOfLevels * 20);
        stats.put(StatType.STR, stats.get(StatType.STR) + nOfLevels * 2);
        stats.put(StatType.DEX, stats.get(StatType.DEX) + nOfLevels * 5);
        stats.put(StatType.INT, stats.get(StatType.INT) + nOfLevels * 1);
    }

    @Override
    public String getName() {
        return "Ranger";
    }
}
