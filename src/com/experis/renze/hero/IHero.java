package com.experis.renze.hero;

import com.experis.renze.armor.IArmor;
import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.IWeapon;

public interface IHero {
    /**
     * Returns the name of the class.
     */
    String className();
    /**
     * Equips the provided weapon.
     */
    void equipWeapon(IWeapon weapon);

    /**
     * Equips the provided armor.
     */
    void equipArmor(IArmor armor);

    /**
     * Returns the total amount of attack power the Hero currently has.
     */
    int getAttackPower();

    /**
     * Gets the total stat of the character, items included.
     */
    int getTotalStat(StatType statType);

    void gainXP(int xp);
    void takeDamage(int damage);
    void healToFull();
    int getLevel();
    int getCurrentHealth();
    boolean isAlive();
    String getName();

}
