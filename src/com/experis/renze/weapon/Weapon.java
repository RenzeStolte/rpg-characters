package com.experis.renze.weapon;

import com.experis.renze.enums.StatType;
import com.experis.renze.weapon.types.IWeaponType;

public class Weapon implements IWeapon{

    IWeaponType weaponType;
    String name;
    int level;
    int weaponPower;

    public Weapon(int level, String name, IWeaponType weaponType) {
        this.level = level;
        this.name = name;
        this.weaponType = weaponType;
        this.weaponPower = weaponType.getWeaponPower(level);
    }

    @Override
    public String getItemName() {
        return name;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public double getStatMultiplier() {
        return weaponType.getWeaponMultiplier();
    }

    @Override
    public String getWeaponType() {
        return weaponType.getWeaponType();
    }

    @Override
    public StatType getAttackStat() {
        return weaponType.getWeaponStat();
    }

    @Override
    public int getAttackPower() {
        return weaponPower;
    }


}
