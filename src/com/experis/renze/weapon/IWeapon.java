package com.experis.renze.weapon;

import com.experis.renze.enums.StatType;

public interface IWeapon {

    public String getItemName();
    public int getLevel();

    /**
     * Returns the multiplier of the weapon.
     */
    public double getStatMultiplier();

    /**
     * Returns the name of the weapon type.
     */
    public String getWeaponType();

    /**
     * Gets the stat type for the weapon
     * @return StatType of weapon
     */
    public StatType getAttackStat();

    /**
     * @return total attack power of item
     */
    public int getAttackPower();




}
