package com.experis.renze.weapon.types;

import com.experis.renze.enums.StatType;

public class RangedWeapon implements IWeaponType {

    @Override
    public double getWeaponMultiplier() {
        return 2;
    }

    @Override
    public int getWeaponPower(int level) {
        return 5 + (3 * (level - 1));
    }

    @Override
    public StatType getWeaponStat() {
        return StatType.DEX;
    }

    @Override
    public String getWeaponType() {
        return "Ranged";
    }
}
