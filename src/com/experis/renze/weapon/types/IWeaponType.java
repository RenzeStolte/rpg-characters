package com.experis.renze.weapon.types;

import com.experis.renze.enums.StatType;

public interface IWeaponType {
    public double getWeaponMultiplier();
    public int getWeaponPower(int level);
    public StatType getWeaponStat();
    public String getWeaponType();
}
