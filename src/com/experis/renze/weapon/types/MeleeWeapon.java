package com.experis.renze.weapon.types;

import com.experis.renze.enums.StatType;

public class MeleeWeapon implements IWeaponType {

    @Override
    public double getWeaponMultiplier() {
        return 1.5;
    }

    @Override
    public int getWeaponPower(int level) {
        return 15 + (2 * (level - 1));
    }

    @Override
    public StatType getWeaponStat() {
        return StatType.STR;
    }

    @Override
    public String getWeaponType() {
        return "Melee";
    }
}
