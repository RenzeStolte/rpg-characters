package com.experis.renze.weapon.types;

import com.experis.renze.enums.StatType;

public class MagicWeapon implements IWeaponType {

    @Override
    public double getWeaponMultiplier() {
        return 3;
    }

    @Override
    public int getWeaponPower(int level) {
        return 25 + (2 * (level - 1));
    }

    @Override
    public StatType getWeaponStat() {
        return StatType.INT;
    }

    @Override
    public String getWeaponType() {
        return "Magic";
    }
}
