package com.experis.renze.armor.specializations;

import com.experis.renze.hero.ISpecialization;
import com.experis.renze.enums.StatType;

import java.util.HashMap;

public class PlateArmor implements ISpecialization {
    @Override
    public void setLevelOneStats(HashMap<StatType, Integer> stats) {
        stats.put(StatType.HLT, 30);
        stats.put(StatType.STR, 3);
        stats.put(StatType.DEX, 1);
        stats.put(StatType.INT, 0);
    }

    @Override
    public void levelStats(HashMap<StatType, Integer> stats, int nOfLevels) {
        stats.put(StatType.HLT, stats.get(StatType.HLT) + nOfLevels * 12);
        stats.put(StatType.STR, stats.get(StatType.STR) + nOfLevels * 2);
        stats.put(StatType.DEX, stats.get(StatType.DEX) + nOfLevels * 1);
        stats.put(StatType.INT, stats.get(StatType.INT) + nOfLevels * 0);
    }

    @Override
    public String getName() {
        return "Plate armor";
    }
}
