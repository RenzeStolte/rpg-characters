package com.experis.renze.armor;

import com.experis.renze.hero.ISpecialization;
import com.experis.renze.enums.ItemSlot;
import com.experis.renze.enums.StatType;

import java.util.HashMap;

public class Armor implements IArmor{

    HashMap<StatType, Integer> stats;
    ItemSlot slot;
    String name;
    ISpecialization armorType;
    int level;

    public Armor(ISpecialization armorType, int level, ItemSlot slot, String name) {
        // Set initial values
        this.armorType = armorType;
        this.slot = slot;
        this.level = level;
        this.name = name;

        // Set armor stats
        stats = new HashMap<>();
        armorType.setLevelOneStats(stats);
        armorType.levelStats(stats, level - 1);
    }

    @Override
    public String getArmorName() {
        return name;
    }

    @Override
    public String getArmorType() {
        return armorType.getName();
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public int getStat(StatType statType) {
        // Scale the stat based on which slot they are equipped in.
        return (int) (stats.get(statType) * SlotEffectiveness.getSlotEffectiveness(slot));
    }

    @Override
    public ItemSlot getSlotType() {
        return slot;
    }
}
