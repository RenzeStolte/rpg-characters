package com.experis.renze.armor;

import com.experis.renze.enums.ItemSlot;

public class SlotEffectiveness {
    /**
     * Returns the amount that a stat should be scaled by based on which slot it is equipped in.
     */
    public static double getSlotEffectiveness(ItemSlot slot){
        switch(slot) {
            case HEAD -> {
                return 0.8;
            }
            case BODY -> {
                return 1;
            }
            case LEGS -> {
                return 0.6;
            }
        }
        return -1;
    }
}
