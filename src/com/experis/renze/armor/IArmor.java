package com.experis.renze.armor;

import com.experis.renze.enums.ItemSlot;
import com.experis.renze.enums.StatType;

public interface IArmor {
    /**
     *  Returns the name of the armor.
     */
    public String getArmorName();

    /**
     * Returns the name of the armor type.
     */
    public String getArmorType();

    /**
     * Returns the number how many levels the armor has.
     */
    public int getLevel();

    /**
     * Returns the stat of the item that is specified in the parameter statType.
     */
    public int getStat(StatType statType);

    /**
     * Returns in which slot this armor fits.
     */
    public ItemSlot getSlotType();

}
