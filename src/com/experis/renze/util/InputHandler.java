package com.experis.renze.util;

import java.io.IOError;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class InputHandler {

    /**
     * Waits for user input before continuing.
     */
    public static void getAnyInput() {
        Scanner scanner = null;
        scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    /**
     * Lets the user choose from some options. User can choose by select 1 up to maxChoices.
     * Query needs to provide directions for the user.
     */
    public static int chooseFromOptions(String query, int maxChoices) {
        boolean validInput = false;
        int userInput = 0;
        while(!validInput) {
            userInput = InputHandler.getIntFromUser(query);
            if(userInput > maxChoices || userInput < 1) {
                System.out.println("That is not a valid option, please select again.");
            } else {
                validInput = true;
            }
        }
        return userInput;
    }


    /**
     * Keeps asking the provided query until the user has given a valid integer.
     */
    public static int getIntFromUser(String query) {
        int userInt = -1;
        Scanner scanner = new Scanner(System.in);
        boolean noValidInput = true;

        while(noValidInput) {
            try {
                OutputHandler.printString(query);
                userInt = scanner.nextInt();
                noValidInput = false;
            } catch (NoSuchElementException e) {
                OutputHandler.printString("Please enter a valid integer.");
                scanner.nextLine();
            }
        }
        return userInt;
    }

    /**
     * Keeps asking the provided query until the user has responded with a string.
     */
    public static String getStringFromUser(String query) {
        String userResponse = "";
        Scanner scanner = new Scanner(System.in);
        boolean noValidInput = true;
        while(noValidInput) {
            try {
                OutputHandler.printString(query);
                userResponse = scanner.next();
                noValidInput = false;
            } catch (NoSuchElementException e) {
                System.out.println("Please enter a valid response.");
                scanner.nextLine();
            }
        }

        return userResponse;
    }
}
