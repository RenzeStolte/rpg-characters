package com.experis.renze.util;

import java.io.IOException;

public class OutputHandler {
    /**
     * Prints a string to the console.
     */
    public static void printString(String string) {
        System.out.println(string);
    }

    public static void clearScreen() {
        try {
            Runtime.getRuntime().exec("cls");
        } catch(IOException e) {
            printString("Error when trying to clear the screen: " + e);
        }
    }

    /**
     * Prints all the items in an array of strings to the console.
     */
    public static void printArray(String[] strings) {
        for (String string : strings) {
            System.out.println(string);
        }
    }
}
