package com.experis.renze.enums;

/**
 * Name of all the stats a hero or items can have.
 */
public enum StatType {
    DEX,
    INT,
    STR,
    HLT
}
