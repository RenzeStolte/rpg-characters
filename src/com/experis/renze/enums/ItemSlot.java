package com.experis.renze.enums;

/**
 * Name of all the slots where you can equip armor
 */
public enum ItemSlot {
    HEAD,
    BODY,
    LEGS
}
