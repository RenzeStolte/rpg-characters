package com.experis.renze;

import com.experis.renze.demo.BattleController;
import com.experis.renze.demo.Demonstration;
import com.experis.renze.demo.DuelMode;
import com.experis.renze.util.InputHandler;
import com.experis.renze.util.OutputHandler;

public class Main {

    public static void main(String[] args) {
        Boolean runProgram = true;
        while(runProgram) {
            StringBuilder str = new StringBuilder();
            str.append("Insert number of desired action: \n");
            str.append("1 - Show demonstration of features.\n");
            str.append("2 - Start duel mode.\n");
            str.append("3 - quit application\n");

            int userChoice = InputHandler.getIntFromUser(str.toString());

            switch (userChoice) {
                case 1 -> Demonstration.demonstrate();
                case 2 -> DuelMode.StartDuelMode();
                case 3 -> {
                    OutputHandler.printString("Now exiting the program...");
                    runProgram = false;
                }
                default -> OutputHandler.printString("That is not a valid option.");
            }
        }
    }
}
