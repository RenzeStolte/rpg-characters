package com.experis.renze.demo;

import com.experis.renze.hero.IHero;
import com.experis.renze.util.InputHandler;
import com.experis.renze.util.OutputHandler;

public class BattleController {

    public static void duel(IHero heroOne, IHero heroTwo) {
        int turnNumber = 1;
        OutputHandler.printString("Start of battle between " + heroOne.getName() + " and " + heroTwo.getName() + ".");

        // Battle each other until one or both heroes are dead.
        while(heroOne.isAlive() && heroTwo.isAlive()) {
            InputHandler.getAnyInput();
            OutputHandler.printString("Start of turn " + turnNumber + ".");
            printHealth(heroOne, heroTwo);
            int attackOne = heroOne.getAttackPower();
            int attackTwo = heroTwo.getAttackPower();
            printDamage(heroOne.getName(), attackOne);
            printDamage(heroTwo.getName(), attackTwo);

            heroOne.takeDamage(attackTwo);
            heroTwo.takeDamage(attackOne);
            turnNumber++;
        }

        // Check who died, and print out a message.
        if(!heroOne.isAlive() && !heroTwo.isAlive()) {
            OutputHandler.printString(heroOne.getName() + " and " + heroTwo.getName() + " knocked eachother out!");
        } else if(!heroOne.isAlive()) {
            OutputHandler.printString(heroTwo.getName() + " knocked out " + heroOne.getName() + "!");
        } else {
            OutputHandler.printString(heroOne.getName() + " knocked out " + heroTwo.getName() + "!");
        }
    }

    /**
     * Print health of both heroes
     */
    private static void printHealth(IHero heroOne, IHero heroTwo) {
        OutputHandler.printString(heroOne.getName() + " health: " + heroOne.getCurrentHealth());
        OutputHandler.printString(heroTwo.getName() + " health: " + heroTwo.getCurrentHealth());
    }

    /**
     * Print a message about how much damage a hero is going to do.
     */
    private static void printDamage(String heroName, int attackPower){
        OutputHandler.printString(heroName + " is going to attack with " + attackPower + " power!");
    }
}
