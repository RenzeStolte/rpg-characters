package com.experis.renze.demo;

import com.experis.renze.armor.IArmor;
import com.experis.renze.hero.IHero;
import com.experis.renze.enums.StatType;
import com.experis.renze.util.OutputHandler;
import com.experis.renze.weapon.IWeapon;

public class DisplayStats {
    public static void displayCharacterStats(IHero hero) {
        StringBuilder sb = new StringBuilder();
        sb.append("Stats for " + hero.getName() + ":\n");
        sb.append("Class: " + hero.className() + "\n");
        sb.append("HP: " + hero.getCurrentHealth() + "/" + hero.getTotalStat(StatType.HLT) + "\n");
        sb.append("Str: " + hero.getTotalStat(StatType.STR) + "\n");
        sb.append("Dex: " + hero.getTotalStat(StatType.DEX) + "\n");
        sb.append("Int: " + hero.getTotalStat(StatType.INT) + "\n");
        sb.append("Lvl: " + hero.getLevel() + "\n");
        OutputHandler.printString(sb.toString());
    }

    public static void displayWeaponsStats(IWeapon weapon) {
        StringBuilder sb = new StringBuilder();
        sb.append("Item stats for: " + weapon.getItemName() + "\n");
        sb.append("Weapon type: " + weapon.getWeaponType() + "\n");
        sb.append("Weapon level: " + weapon.getLevel() + "\n");
        sb.append("Damage: " + weapon.getAttackPower() + "\n");
        OutputHandler.printString(sb.toString());
    }

    public static void displayArmorStats(IArmor armor) {
        StringBuilder sb = new StringBuilder();
        sb.append("Item stats for: " + armor.getArmorName() + "\n");
        sb.append("Armor type: " + armor.getArmorType() + "\n");
        sb.append("Slot: " + armor.getSlotType() + "\n");
        sb.append("Armor level: " + armor.getLevel() + "\n");
        sb.append("Bonus hp: " + armor.getStat(StatType.HLT) + "\n");
        sb.append("Bonus strength: " + armor.getStat(StatType.STR) + "\n");
        sb.append("Bonus dex: " + armor.getStat(StatType.DEX) + "\n");
        sb.append("Bonus int: " + armor.getStat(StatType.INT) + "\n");
        OutputHandler.printString(sb.toString());
    }
}
