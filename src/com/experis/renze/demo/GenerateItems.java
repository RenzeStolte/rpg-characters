package com.experis.renze.demo;

import com.experis.renze.armor.Armor;
import com.experis.renze.armor.IArmor;
import com.experis.renze.armor.specializations.ClothArmor;
import com.experis.renze.armor.specializations.LeatherArmor;
import com.experis.renze.armor.specializations.PlateArmor;
import com.experis.renze.enums.ItemSlot;
import com.experis.renze.hero.Hero;
import com.experis.renze.hero.IHero;
import com.experis.renze.hero.ISpecialization;
import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.hero.specializations.MageSpecialization;
import com.experis.renze.hero.specializations.RangerSpecialization;
import com.experis.renze.hero.specializations.WarriorSpecialization;
import com.experis.renze.weapon.IWeapon;
import com.experis.renze.weapon.Weapon;
import com.experis.renze.weapon.types.MagicWeapon;
import com.experis.renze.weapon.types.MeleeWeapon;
import com.experis.renze.weapon.types.RangedWeapon;

import java.util.Random;

public class GenerateItems {

    /**
     * Generate and return a hero with random class and items.
     */
    public static IHero generateRandomHero(int level) {
        Random rand = new Random();

        // Set class
        ISpecialization heroClass;
        switch(rand.nextInt(3)){
            case 0 -> heroClass = new MageSpecialization();
            case 1 -> heroClass = new RangerSpecialization();
            case 2 -> heroClass = new WarriorSpecialization();
            default -> heroClass = null;
        }
        IHero hero = new Hero(heroClass, level, "Bad boi", new EquipmentHandler());

        // Get weapon
        IWeapon weapon;
        switch(rand.nextInt(3)){
            case 0 -> weapon = generateMeleeWeapon(level);
            case 1 -> weapon = generateRangedWeapon(level);
            case 2 -> weapon = generateMagicWeapon(level);
            default -> weapon = null;
        }

        // Get armor
        IArmor legs = generateLegArmor(level);
        IArmor head = generateHeadArmor(level);
        IArmor body = generateBodyArmor(level);

        // Equip items and return
        hero.equipWeapon(weapon);
        hero.equipArmor(legs);
        hero.equipArmor(head);
        hero.equipArmor(body);
        return hero;
    }

    public static IArmor generateLegArmor(int level) {
        ISpecialization armorClass = getRandomArmorClass();
        return new Armor(armorClass, level, ItemSlot.LEGS, "Leggings");
    }

    public static IArmor generateHeadArmor(int level) {
        ISpecialization armorClass = getRandomArmorClass();
        return new Armor(armorClass, level, ItemSlot.HEAD, "Helmet");
    }

    public static IArmor generateBodyArmor(int level) {
        ISpecialization armorClass = getRandomArmorClass();
        return new Armor(armorClass, level, ItemSlot.BODY, "Body");
    }

    private static ISpecialization getRandomArmorClass() {
        Random rand = new Random();
        ISpecialization armorClass;
        switch (rand.nextInt(3)) {
            case 0 -> armorClass = new PlateArmor();
            case 1 -> armorClass = new ClothArmor();
            case 2 -> armorClass = new LeatherArmor();
            default -> armorClass = null;
        }
        return armorClass;
    }

    /**
     * Generate melee weapon of given level with a random name.
     */
    public static IWeapon generateMeleeWeapon(int level) {
        String name = meleeWeaponNames() + " of the " + lastName();
        return new Weapon(level, name, new MeleeWeapon());
    }

    /**
     * Generate ranged weapon of given level with a random name.
     */
    public static IWeapon generateRangedWeapon(int level) {
        String name = rangedWeaponNames() + " of the " + lastName();
        return new Weapon(level, name, new RangedWeapon());
    }

    /**
     * Generate magic weapon of given level with a random name.
     */
    public static IWeapon generateMagicWeapon(int level) {
        String name = magicWeaponNames() + " of the " + lastName();
        return new Weapon(level, name, new MagicWeapon());
    }

    public static IArmor generatePlateArmor(int level) {

        //return new Armor(new PlateArmor(), )
        return null;
    }

    /**
     * Returns a random name for a ranged weapon.
     */
    private static String magicWeaponNames(){
        Random rand = new Random();
        int randomNumber = rand.nextInt(6);
        switch (randomNumber) {
            case 0 -> {
                return "Holy Scriptures";
            }
            case 1 -> {
                return "Wand";
            }
            case 2 -> {
                return "Staff";
            }
            case 3 -> {
                return "Relic";
            }
            case 4 -> {
                return "Skull";
            }
            case 5 -> {
                return "Impressive Trinket";
            }
            default -> {
                return "Default";
            }
        }
    }

    /**
     * Returns a random name for a ranged weapon.
     */
    private static String rangedWeaponNames(){
        Random rand = new Random();
        int randomNumber = rand.nextInt(6);
        switch (randomNumber) {
            case 0 -> {
                return "Long Bow";
            }
            case 1 -> {
                return "Short Bow";
            }
            case 2 -> {
                return "Combat Sniper";
            }
            case 3 -> {
                return "Slingshot";
            }
            case 4 -> {
                return "Snowball";
            }
            case 5 -> {
                return "Javelin";
            }
            default -> {
                return "Default";
            }
        }
    }

    /**
     * Returns a random name for a melee weapon.
     */
    private static String meleeWeaponNames(){
        Random rand = new Random();
        int randomNumber = rand.nextInt(6);
        switch (randomNumber) {
            case 0 -> {
                return "Great Axe";
            }
            case 1 -> {
                return "Broadsword";
            }
            case 2 -> {
                return "Lance";
            }
            case 3 -> {
                return "Claymore";
            }
            case 4 -> {
                return "Mace";
            }
            case 5 -> {
                return "Twin-Blades";
            }
            default -> {
                return "Default";
            }
        }
    }

    /**
     * Returns a random name that is meant to be used at the end.
     */
    private static String lastName() {
        Random rand = new Random();
        int randomNumber = rand.nextInt(9);
        switch (randomNumber) {
            case 0 -> {
                return "Fallen Goddess";
            }
            case 1 -> {
                return "Exalted";
            }
            case 2 -> {
                return "Invincible";
            }
            case 3 -> {
                return "Betrayer";
            }
            case 4 -> {
                return "Broken One";
            }
            case 5 -> {
                return "Magi";
            }
            case 6 -> {
                return "Battle Born";
            }
            case 7 -> {
                return "Forgotten";
            }
            case 8 -> {
                return "Revered";
            }
            default -> {
                return "Default";
            }
        }
    }

}
