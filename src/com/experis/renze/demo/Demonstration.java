package com.experis.renze.demo;

import com.experis.renze.armor.Armor;
import com.experis.renze.armor.IArmor;
import com.experis.renze.armor.specializations.ClothArmor;
import com.experis.renze.armor.specializations.LeatherArmor;
import com.experis.renze.armor.specializations.PlateArmor;
import com.experis.renze.hero.Hero;
import com.experis.renze.hero.IHero;
import com.experis.renze.hero.specializations.MageSpecialization;
import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.enums.ItemSlot;
import com.experis.renze.util.InputHandler;
import com.experis.renze.util.OutputHandler;
import com.experis.renze.weapon.IWeapon;
import com.experis.renze.weapon.Weapon;
import com.experis.renze.weapon.types.MagicWeapon;
import com.experis.renze.weapon.types.MeleeWeapon;
import com.experis.renze.weapon.types.RangedWeapon;

public class Demonstration {

    public static void demonstrate() {
        // Show creation of different items.
        OutputHandler.printString("Creating melee weapon...");
        IWeapon axe = new Weapon(1, "Great axe of the forgotten", new MeleeWeapon());
        DisplayStats.displayWeaponsStats(axe);
        InputHandler.getAnyInput();

        OutputHandler.printString("Creating ranged weapon...");
        IWeapon bow = new Weapon(6, "Long Bow of the Eagle", new RangedWeapon());
        DisplayStats.displayWeaponsStats(bow);
        InputHandler.getAnyInput();

        OutputHandler.printString("Creating magic weapon...");
        IWeapon staff = new Weapon(4, "Staff of the Weathered Rock", new MagicWeapon());
        DisplayStats.displayWeaponsStats(staff);
        InputHandler.getAnyInput();

        OutputHandler.printString("Creating chest plate...");
        IArmor chestPlate = new Armor(new PlateArmor(), 15, ItemSlot.BODY, "Chest Plate of the Unmovable");
        DisplayStats.displayArmorStats(chestPlate);
        InputHandler.getAnyInput();

        OutputHandler.printString("Creating leather leggings...");
        IArmor leatherLeggings = new Armor(new LeatherArmor(), 15, ItemSlot.LEGS, "Leather Leggings of the Lion");
        DisplayStats.displayArmorStats(leatherLeggings);
        InputHandler.getAnyInput();

        OutputHandler.printString("Creating a cloth hat...");
        IArmor clothHat = new Armor(new ClothArmor(), 15, ItemSlot.HEAD, "Cloth Cap of the Deceiver");
        DisplayStats.displayArmorStats(clothHat);
        InputHandler.getAnyInput();

        // Show initial stats.
        IHero hero = new Hero(new MageSpecialization(), 1, "Gandalf", new EquipmentHandler());
        DisplayStats.displayCharacterStats(hero);
        InputHandler.getAnyInput();

        // Level twice and show stats.
        OutputHandler.printString("Adding 2 levels to the character...");
        hero.gainXP(210);
        DisplayStats.displayCharacterStats(hero);
        InputHandler.getAnyInput();

        // Equip all items and show that stats have changed.
        OutputHandler.printString("Equipping all three armor pieces on " +  hero.getName() + "...\n");
        hero.equipArmor(clothHat);
        hero.equipArmor(leatherLeggings);
        hero.equipArmor(chestPlate);
        DisplayStats.displayCharacterStats(hero);
        InputHandler.getAnyInput();

        // Show that equipping an item with lower stats changes the hero's stats correctly.
        OutputHandler.printString("Equipping a cloth vest instead...");
        IArmor clothVest = new Armor(new ClothArmor(), 1, ItemSlot.BODY, "Cloth Vest of the Sheep");
        hero.equipArmor(clothVest);
        DisplayStats.displayCharacterStats(hero);
        InputHandler.getAnyInput();

        equipAndDisplayAttackPower(bow, hero);

        equipAndDisplayAttackPower(axe, hero);

        equipAndDisplayAttackPower(staff, hero);
    }

    /**
     * Equip an item, and then show what the attack power would be.
     */
    private static void equipAndDisplayAttackPower(IWeapon weapon, IHero hero) {
        OutputHandler.printString("Equipping " + weapon.getItemName());
        hero.equipWeapon(weapon);
        OutputHandler.printString(hero.getName() + " would attack for " + hero.getAttackPower() + " with the current weapon.");
        InputHandler.getAnyInput();
    }
}
