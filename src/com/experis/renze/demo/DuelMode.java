package com.experis.renze.demo;

import com.experis.renze.armor.IArmor;
import com.experis.renze.hero.IHero;
import com.experis.renze.util.InputHandler;
import com.experis.renze.util.OutputHandler;
import com.experis.renze.weapon.IWeapon;

import java.util.Random;

public class DuelMode {

    public static void StartDuelMode(){
        // Create hero for user
        IHero userHero = CreateUserHero.createHero();

        // Show stats
        OutputHandler.printString("\nYour hero's stats:");
        DisplayStats.displayCharacterStats(userHero);
        InputHandler.getAnyInput();

        // Generate and show their armor
        OutputHandler.printString("This is your starting armor:");
        OutputHandler.printString("-------------------------------------------------\n");
        IArmor body = GenerateItems.generateBodyArmor(1);
        DisplayStats.displayArmorStats(body);
        userHero.equipArmor(body);
        IArmor legs = GenerateItems.generateLegArmor(1);
        DisplayStats.displayArmorStats(legs);
        userHero.equipArmor(legs);
        IArmor head = GenerateItems.generateHeadArmor(1);
        DisplayStats.displayArmorStats(head);
        userHero.equipArmor(head);
        InputHandler.getAnyInput();

        // Show stats again
        OutputHandler.printString("\nYour hero's stats:");
        DisplayStats.displayCharacterStats(userHero);
        InputHandler.getAnyInput();

        // Let them choose weapon
        chooseStartingWeapon(userHero);

        // Start dueling
        OutputHandler.printString("Character creation complete!");

        while(userHero.isAlive()) {

            OutputHandler.printString("\nYour current stats: ");
            DisplayStats.displayCharacterStats(userHero);
            InputHandler.getAnyInput();

            OutputHandler.printString("You will now be allowed to make two actions from the following options:");

            for (int i = 0; i < 2; i++) {
                StringBuilder strB = new StringBuilder();
                strB.append("Choose what you want to do:\n");
                strB.append("1 - Heal to full.\n");
                strB.append("2 - Get a new weapon.\n");
                strB.append("3 - Get a new armor piece.\n");
                int userChoice = InputHandler.chooseFromOptions(strB.toString(), 3);
                switch(userChoice) {
                    case 1 -> userHero.healToFull();
                    case 2 -> getNewWeapon(userHero);
                    case 3 -> getNewArmor(userHero);
                }
            }

            doBattle(userHero);
            userHero.gainXP(25);
        }

        OutputHandler.printString("\n---------------------------------------------------");
        OutputHandler.printString("         Game over, bad luck!");
        OutputHandler.printString("---------------------------------------------------");
    }

    /**
     * Generates a armor piece, then asks the user if they want to equip it
     * If yes, equips it.
     */
    private static void getNewArmor(IHero hero){
        Random rand = new Random();
        IArmor armor;
        // Generate random piece of armor and display it to the user
        switch(rand.nextInt(3)) {
            case 0 -> armor = GenerateItems.generateBodyArmor(hero.getLevel());
            case 1 -> armor = GenerateItems.generateLegArmor(hero.getLevel());
            case 2 -> armor = GenerateItems.generateHeadArmor(hero.getLevel());
            default -> armor = null;
        }
        DisplayStats.displayArmorStats(armor);

        // Ask if they want to equip it
        OutputHandler.printString("Do you want to equip this armor piece?");

        int userChoice = InputHandler.getIntFromUser("Press 0 for yes, any other number for no.");
        if(userChoice == 0)
            hero.equipArmor(armor);
    }

    /**
     * Generates a weapon, then asks the user if they want to equip it.
     * If they answer yes, it is equipped
     */
    private static void getNewWeapon(IHero hero) {
        Random rand = new Random();
        // Generate random weapon type and display it to user
        IWeapon weapon;
        switch(rand.nextInt(3)) {
            case 0 -> weapon = GenerateItems.generateMagicWeapon(hero.getLevel());
            case 1 -> weapon = GenerateItems.generateMeleeWeapon(hero.getLevel());
            case 2 -> weapon = GenerateItems.generateRangedWeapon(hero.getLevel());
            default -> weapon = null;
        }
        DisplayStats.displayWeaponsStats(weapon);

        // Ask if they want to equip it
        OutputHandler.printString("Do you want to equip this weapon?");
        int userChoice = InputHandler.getIntFromUser("Press 0 for yes, any other number for no.");
        if(userChoice == 0)
            hero.equipWeapon(weapon);
    }

    /**
     * Generates an enemy, and lets them battle the provided hero
     */
    private static void doBattle(IHero userHero) {
        IHero hero = GenerateItems.generateRandomHero(userHero.getLevel());
        BattleController.duel(userHero, hero);
    }

    /**
     * Let the user choose between three weapons of different weapon types and equips it.
     */
    private static void chooseStartingWeapon(IHero hero) {
        OutputHandler.printString("Choose a weapon: ");

        IWeapon mageWeapon = GenerateItems.generateMagicWeapon(1);
        DisplayStats.displayWeaponsStats(mageWeapon);
        IWeapon rangedWeapon = GenerateItems.generateRangedWeapon(1);
        DisplayStats.displayWeaponsStats(rangedWeapon);
        IWeapon meleeWeapon = GenerateItems.generateMeleeWeapon(1);
        DisplayStats.displayWeaponsStats(meleeWeapon);

        int userChoice = InputHandler.chooseFromOptions("Press 1, 2 or 3 for the weapon of your choice.", 3);
        switch (userChoice){
            case 1 -> hero.equipWeapon(mageWeapon);
            case 2 -> hero.equipWeapon(rangedWeapon);
            case 3 -> hero.equipWeapon(meleeWeapon);
            default -> throw new IllegalStateException("Unexpected value: " + userChoice);
        }
    }
}
