package com.experis.renze.demo;

import com.experis.renze.hero.Hero;
import com.experis.renze.hero.IHero;
import com.experis.renze.hero.ISpecialization;
import com.experis.renze.hero.inventory.EquipmentHandler;
import com.experis.renze.hero.specializations.MageSpecialization;
import com.experis.renze.hero.specializations.RangerSpecialization;
import com.experis.renze.hero.specializations.WarriorSpecialization;
import com.experis.renze.util.InputHandler;
import com.experis.renze.util.OutputHandler;
import org.w3c.dom.ranges.Range;

public class CreateUserHero {
    public static IHero createHero(){
        String name = selectName();
        ISpecialization heroSpecialization = selectClass();
        return new Hero(heroSpecialization, 1, name, new EquipmentHandler());
    }

    private static ISpecialization selectClass(){
        StringBuilder strB = new StringBuilder();
        strB.append("Select which class you want to play as: \n");
        strB.append("1 - Mage\n");
        strB.append("2 - Warrior\n");
        strB.append("3 - Ranger\n");
        int classChoice = InputHandler.chooseFromOptions(strB.toString(), 3);
        switch(classChoice) {
            case 1 -> {
                return new MageSpecialization();
            }
            case 2 -> {
                return new WarriorSpecialization();
            }
            case 3 -> {
                return new RangerSpecialization();
            }
            default -> throw new IllegalStateException("Unexpected value: " + classChoice);
        }
    }

    private static String selectName(){
        return InputHandler.getStringFromUser("Enter the name of your hero: ");
    }
}
